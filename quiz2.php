<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        session_start();
        $ans_page_1 = $_SESSION;

        $question = array(
            1 => "Which country has 3 capitals?", 
            2 => "Which country is the most populous in the world?",
            3 => "Which country is the largest in the world?", 
            4 => "What is the capital of Thailand?",
            5 => "How many ethnic groups does Vietnam have?", 
            6 => "Which country has 3 capitals?", 
            7 => "Which country is the most populous in the world?",
            8 => "Which country is the largest in the world?", 
            9 => "What is the capital of Thailand?",
            10 => "How many ethnic groups does Vietnam have?",);

        $answer = array(
            1 => array("South Africa"=>1, "Russia"=>0, "Thailand"=>0, "Korea"=>0), 
            2 => array("India"=>0, "Indonesia"=>0, "China"=>1, "Australia"=>0),
            3 => array("Russia"=>1, "China"=>0, "America"=>0, "India"=>0),
            4 => array("Phuket"=>0, "Chiangmai"=>0, "Bangkok"=>1, "Pattaya"=>0),
            5 => array("54"=>1, "53"=>0, "55"=>0, "50"=>0), 
            6 => array("South Africa"=>1, "Russia"=>0, "Thailand"=>0, "Korea"=>0), 
            7 => array("India"=>0, "Indonesia"=>0, "China"=>1, "Australia"=>0),
            8 => array("Russia"=>1, "China"=>0, "America"=>0, "India"=>0),
            9 => array("Phuket"=>0, "Chiangmai"=>0, "Bangkok"=>1, "Pattaya"=>0),
            10 => array("54"=>1, "53"=>0, "55"=>0, "50"=>0), );

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $_SESSION = $ans_page_1;
            for ($i=6; $i <= 10; $i++) { 
                $key = "question_".strval($i);
                $_SESSION[$key]=$_POST["$key"];
            }
            $_SESSION["answer"] = $answer;
            header("Location: submit.php");
        }
    ?>
    <form method="POST" enctype="multipart/form-data" action="<?php 
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <h1> Trắc nghiệm</h1>
        <?php
            foreach ($question as $key => $value) {
                if ($key >= 6){
                    echo "<p>Câu {$key}: {$value}</p>";
                    foreach ($answer as $key_ans => $value_ans) {
                        if ($key == $key_ans){
                            foreach ($value_ans as $vlue => $dan){
                                echo "<input type=\"radio\" name=\"question_{$key_ans}\" value=\"{$vlue}\"> {$vlue}<br>";
                            }
                        }
                    }
                }
            };
        ?>
        <button>Nộp bài</button>
    </form>
</body>
</html>